import { server } from 'src/config';
import Link from 'next/link';
import { Meta } from '@components/Meta';
import { IArticleItem } from '@components/Article/Article.types';

const ArticlePage = ({ article }) => {
  return (
    <>
      <Meta title={article.title} description={article.excerpt} />
      <h1>{article.title}</h1>
      <p>{article.body}</p>
      <br />
      <Link href="/">Go Back</Link>
    </>
  );
};

export const getStaticProps = async context => {
  const res = await fetch(`${server}/api/articles/${context.params.id}`, {
    headers: {
      Accept: 'application/json, text/plain, */*',
      'User-Agent': '*',
    },
  });
  const article: IArticleItem = await res.json();
  return {
    props: {
      article,
    },
  };
};

export const getStaticPaths = async () => {
  const res = await fetch(`${server}/api/articles`, {
    headers: {
      Accept: 'application/json, text/plain, */*',
      'User-Agent': '*',
    },
  });

  const articles: IArticleItem[] = await res.json();
  const ids = articles.map(article => article.id);
  const paths = ids.map((id: string) => ({
    params: { id: id.toString() },
  }));
  return {
    paths,
    fallback: false,
  };
};

export default ArticlePage;
