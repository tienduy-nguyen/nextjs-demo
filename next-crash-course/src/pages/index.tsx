import { IArticlesProps } from '@components/Article/Article.types';
import { ArticleList } from '@components/Article/ArticleList';
import { server } from '../config';

export default function Home(props: IArticlesProps) {
  const { articles } = props;
  return (
    <div>
      <ArticleList articles={articles} />
    </div>
  );
}

export const getStaticProps = async () => {
  const res = await fetch(`${server}/api/articles`, {
    headers: {
      Accept: 'application/json, text/plain, */*',
      'User-Agent': '*',
    },
  });
  const articles = await res.json();

  return {
    props: {
      articles,
    },
  };
};
