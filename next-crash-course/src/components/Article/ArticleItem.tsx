import Link from 'next/link';
import styles from './Article.module.css';
import { IArticlesProps } from './Article.types';

export const ArticleItem = (props: IArticlesProps) => {
  const { article } = props;
  return (
    <Link href="/article/[id]" as={`/article/${article.id}`}>
      <a className={styles.card}>
        <h3>{article.title} &rarr;</h3>
        <p>{article.excerpt}</p>
      </a>
    </Link>
  );
};
