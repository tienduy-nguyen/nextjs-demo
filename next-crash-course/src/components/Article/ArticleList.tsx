import { ArticleItem } from './ArticleItem';
import styles from './Article.module.css';
import { IArticlesProps } from './Article.types';

export const ArticleList = (props: IArticlesProps) => {
  const { articles } = props;
  return (
    <div className={styles.grid}>
      {articles.map(article => (
        <ArticleItem article={article} key={article.id} />
      ))}
    </div>
  );
};
