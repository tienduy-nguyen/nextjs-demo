export interface IArticleItem {
  id: string;
  title: string;
  excerpt: string;
  body: string;
}
export interface IArticlesProps {
  article?: IArticleItem;
  articles?: IArticleItem[];
}
