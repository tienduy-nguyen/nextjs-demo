import { Nav } from 'src/components/Nav';
import { Meta } from 'src/components/Meta';
import { Header } from 'src/components/Header';
import styles from './Layout.module.css';

export const Layout = ({ children }) => {
  return (
    <>
      <Meta />
      <Nav />
      <div className={styles.container}>
        <main className={styles.main}>
          <Header />
          {children}
        </main>
      </div>
    </>
  );
};
