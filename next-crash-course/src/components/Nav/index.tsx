import Link from 'next/link';
import navStyle from './Nav.module.css';

export const Nav = () => {
  return (
    <nav className={navStyle.nav}>
      <ul>
        <li>
          <Link href="/">Home</Link>
        </li>

        <li>
          <Link href="/about">About</Link>
        </li>
      </ul>
    </nav>
  );
};
